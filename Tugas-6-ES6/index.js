// soal 1
// arrow function

// const hitung = (panjang, lebar) => {
//     let luas = panjang * lebar;
//     let keliling = 2 * (panjang + lebar);

//     return "luas = " + luas + "\n" + "keliling = " + keliling;
// }

// console.log(hitung(2, 4));

// ===================================================================

// soal 2
// arrow function dan object literal

// const newFunction = (firstName, lastName) => {
//     return {
//         firstName, 
//         lastName,
//         fullName(){
//             console.log(firstName + " " + lastName);
//         }};
// }

// newFunction("William", "Imoh").fullName();

// ===========================================================

// soal 3
// destructuring

// const newObject = {
//     firstName: "Muhammad",
//     lastName: "Iqbal Mubarok",
//     address: "Jalan Ranamanyar",
//     hobby: "playing football",
// }

// const {firstName, lastName, address, hobby} = newObject;

// console.log(firstName, lastName, address, hobby);

// ==============================================================

// soal 4
// spread operator

// const west = ["Will", "Chris", "Sam", "Holly"];
// const east = ["Gill", "Brian", "Noel", "Maggie"];

// const combined = [...west, ...east];

// console.log(combined);

// ======================================================

// soal 5
// template literal

// const planet = "earth";
// const view = "glass";

// var before = `Lorem ${view} dolor sit amet, consectetur adipiscing elit, ${planet}`;

// console.log(before);

// ===========================================================================================