// Soal 1
// function string

function next_date(tanggal, bulan, tahun) {
    // var data = [];

    var tanggal = tanggal;
    var bulan = bulan;
    var tahun = tahun;
    
    if (tanggal == 28 && bulan == 2) {
        var bulan_baru = bulan + 1;
    }

    if (tanggal == 29 && bulan == 2) {
        var bulan_baru = bulan + 1;
    }

    if (tanggal == 31 || bulan == 12) {
        var tahun_baru = tahun + 1;
    } 

    switch (bulan_baru) {
        case 1: { console.log(tanggal + " Januari " + tahun_baru); break; }
        case 2: { console.log(tanggal + " Februari " + tahun_baru); break; }
        case 3: { console.log(tanggal + " Maret " + tahun_baru); break; }
        case 4: { console.log(tanggal + " April " + tahun_baru); break; }    
        case 5: { console.log(tanggal + " Mei " + tahun_baru); break; }
        case 6: { console.log(tanggal + " Juni " + tahun_baru); break; }
        case 7: { console.log(tanggal + " Juli " + tahun_baru); break; }
        case 8: { console.log(tanggal + " Agustus " + tahun_baru); break; }
        case 9: { console.log(tanggal + " September " + tahun_baru); break; }
        case 10: { console.log(tanggal + " Oktober " + tahun_baru); break; }
        case 11: { console.log(tanggal + " November " + tahun_baru); break; }
        case 12: { console.log(tanggal + " Desember " + tahun_baru); break; }    

        default: { console.log("bulan tidak terdeteksi"); }
    }
}

var tanggal = 29;
var bulan = 2;
var tahun = 2021;

next_date(tanggal, bulan, tahun);

// ===========================================================================

// Soal 2
// Manipulasi string

// function jumlah_kata(str) {
//     var hitung =  str.split(" ").length;

//     console.log(hitung);
// }

// var kalimat_1 = "Halo nama saya Muhammad Iqbal Mubarok";
// var kalimat_2 = "Saya Iqbal";

// jumlah_kata(kalimat_1);
// jumlah_kata(kalimat_2);

// ===============================================================


