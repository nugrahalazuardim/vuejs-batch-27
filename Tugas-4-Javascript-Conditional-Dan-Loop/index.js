// soal 1
// if else conditional 

// var nilai = 75;

// console.log(nilai + "\n");

// jawaban 1

// if (nilai >= 85) {
//     console.log("indeksnya A");
// } else if(nilai >= 75 && nilai < 85) {
//     console.log("indeksnya B");
// } else if(nilai >= 65 && nilai < 75) {
//     console.log("indeksnya C");
// } else if(nilai >= 55 && nilai < 65) {
//     console.log("indeksnya D");
// } else if(nilai < 55) {
//     console.log("indeksnya E");
// } else {
//     console.log("input tidak lengkap");
// }

// ===================================================

// soal 2
// switch case condition

// var tanggal = 26;
// var bulan = 4;
// var tahun = 1995;

// jawaban 2

// switch (bulan) {
//     case 1: { console.log(tanggal + " Januari " + tahun); break; }
//     case 2: { console.log(tanggal + " Februari " + tahun); break; }
//     case 3: { console.log(tanggal + " Maret " + tahun); break; }
//     case 4: { console.log(tanggal + " April " + tahun); break; }    
//     case 5: { console.log(tanggal + " Mei " + tahun); break; }
//     case 6: { console.log(tanggal + " Juni " + tahun); break; }
//     case 7: { console.log(tanggal + " Juli " + tahun); break; }
//     case 8: { console.log(tanggal + " Agustus " + tahun); break; }
//     case 9: { console.log(tanggal + " September " + tahun); break; }
//     case 10: { console.log(tanggal + " Oktober " + tahun); break; }
//     case 11: { console.log(tanggal + " November " + tahun); break; }
//     case 12: { console.log(tanggal + " Desember " + tahun); break; }    

//     default: { console.log("bulan tidak terdeteksi"); }
// }

// =============================================================================

// soal 3
// looping condition 1

// var n = 3;
// var n2 = 7;

// jawaban 
// n = 3

// var hasil = "";

// console.log("n = 3");

// for (var i = 1; i <= n; i++) {
//     for (var j = 1; j <= i; j++) {
//         hasil += "# ";    
//     }
//     hasil += "\n";
// }

// console.log(hasil);

// n = 7

// var hasil2 = "";

// console.log("n = 7");

// for (var i = 1; i <= n2; i++) {
//     for (var j = 1; j <= i; j++) {
//         hasil2 += "# ";
//     }
//     hasil2 += "\n"; 
// }

// console.log(hasil2);

// ================================================

// soal 4
// looping condition 2
// m = 3, 5, 7

// jawaban
var hasil = "";

// for (var m = 1; m <= 3; m++) {
//         if (m == 1) {
//             console.log(m + " - I love programming");
//         } else if (m == 2) {
//             console.log(m + " - I love Javascript");
//         } else if (m == 3) {
//             console.log(m + " - I love VueJS");
//         } else {
//             console.log("=");
//         }        
//     }

// var m = 3;
// var m = 5;
// var m = 7;
var m = 10;
for (var b = 1; b <= m; b++) {
    hasil += b + " - I love programming \n";
    
    if (b == 3) {
        for (var k = 1; k <= b; k++) {
            hasil += '=';
        }
        hasil += '\n';
    } else if (b == 6) {
        for (var k = 1; k <= b; k++) {
            hasil += '=';
        }
        hasil += '\n';
    } else if (b == 9) {
        for (var k = 1; k <= b; k++) {
            hasil += '=';
        }
        hasil += '\n';
    }
}

console.log(hasil);


