// soal 1
// sorting array

// var daftarHewan = ["2. Komodo", "5. Buaya", "3. Cicak", "4. Ular", "1. Tokek"];

// jawaban

// daftarHewan.sort();
// daftarHewan.forEach(function(item) {
//     console.log(item)
// })

// =======================================================================================

// soal 2
// akses data objek

// jawaban

// function introduce(data) {
//     var nama = data.name;
//     var umur = data.age;
//     var alamat = data.address;
//     var hobi = data.hobby;
//     return "Nama saya " + nama + ", umur saya " + umur + " tahun, alamat saya di " + alamat + ", dan saya punya hobby yaitu " + hobi;
// }

// var data = {
//     name: "John",
//     age: 30,
//     address: "Jalan Palesiran",
//     hobby: "Gaming"
// }

// var perkenalan = introduce(data);
// console.log(perkenalan);

// ===============================================================================================================================================

// soal 3
// manipulasi string dengan function

// function hitung_huruf_vokal(huruf) {
//     var teks = huruf.split("");
//     var jumlah_vokal = 0;
//     for(var v = 0; v < teks.length; v++) {
//         if (hurufVokal(teks[v]) === true) jumlah_vokal++;
//     }
//     return jumlah_vokal;
// }

// function hurufVokal(str) {
//     var vokal = ['a', 'i', 'u', 'e', 'o'];

//     return vokal.indexOf(str.toLowerCase()) !== -1;
// }

// var hitung_1 = hitung_huruf_vokal("Muhammad");
// var hitung_2 = hitung_huruf_vokal("Iqbal");

// console.log(hitung_1, hitung_2);

// ===================================================================

// soal 4
// return function

// function hitung(int) {
//     var hasil = 0;

//     hasil = (2 * int) - 2;

//     return hasil;
// }

// console.log(hitung(0));
// console.log(hitung(1));
// console.log(hitung(2));
// console.log(hitung(3));
// console.log(hitung(5));

// ========================================