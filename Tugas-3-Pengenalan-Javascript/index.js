// soal 1
// menggabungkan dua variable

// var pertama = "saya sangat senang hari ini";
// var kedua = "belajar javascript itu keren";

// jawaban 1

// var kata_pertama = pertama.substring(0, 5);
// var kata_kedua = pertama.substring(12, 19);
// var kata_ketiga = kedua.substring(0, 8);
// var kata_keempat = kedua.substring(8, 19);

// var besar = kata_keempat.toUpperCase();

// var kalimat_pertama = (kata_pertama.concat(kata_kedua));
// var kalimat_kedua = (kata_ketiga.concat(besar));

// var kalimat_baru = (kalimat_pertama.concat(kalimat_kedua));

// console.log(kalimat_baru);

// ===================================================================

// soal 2 
// merubah tipe data variable

// var kataPertama = "10";
// var kataKedua = "2";
// var kataKetiga = "4";
// var kataKeempat = "6";

// jawaban 2

// var angkaPertama = parseInt(kataPertama);
// var angkaKedua = parseInt(kataKedua);
// var angkaKetiga = parseInt(kataKetiga);
// var angkaKeempat = parseInt(kataKeempat);

// var hasil = (angkaPertama - angkaKedua - angkaKetiga) * angkaKeempat;

// console.log(hasil);

// ============================================================================

// soal 3
// mengambil index kalimat

// var kalimat = "wah javascript itu keren sekali";

// var kataPertama = kalimat.substring(0, 3);

// jawaban 3

// var kataKedua = kalimat.substring(4, 14);
// var kataKetiga = kalimat.substring(15, 18);
// var kataKeempat = kalimat.substring(19, 24);
// var kataKelima = kalimat.substring(25, 31);

// console.log("Kata Pertama: " + kataPertama);
// console.log("Kata Kedua: " + kataKedua);
// console.log("Kata Ketiga: " + kataKetiga);
// console.log("Kata Keempat: " + kataKeempat);
// console.log("Kata Kelima: " + kataKelima);

// ===========================================================